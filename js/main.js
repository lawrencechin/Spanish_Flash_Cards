// Functional utilities

const curry = ( fn, arity = fn.length ) => {
    const $curry = oldArgs => ( ...newArgs ) => {
        const allArgs = oldArgs.concat( newArgs );
        const count = allArgs.length;
        return count < arity
            ? $curry( allArgs )
            : fn( ...allArgs );
    };
    return $curry([]);
};

const compose = ( ...fns ) => x => fns.reduceRight(( t, f ) => f( t ), x );

const prop = curry(( prop, obj ) => obj[ prop ]);

const setProp = curry(( prop, obj, value ) => obj[ prop ] = value );

const $ = ( selector, multiple ) => {
    const selection = document.querySelectorAll( selector );
    return multiple ? selection : selection[ 0 ];
};

// Site utilities

const eventListener = curry(( event, elem, callBack ) => {
    elem.addEventListener( event, callBack );
});

const clickEvent = eventListener( "click" );

const toggleClass = curry(( elem, className ) =>
    elem.classList.contains( className )
        ? elem.classList.remove( className )
        : elem.classList.add( className )
);

const toggleSections = ( section, className ) => {
    [ ...$( "section", true )].forEach( sec => {
        sec.classList.remove( className );
    });
    toggleClass( section, className );
};

const printData = curry(( elem, data, callback = null ) => {
    elem.innerHTML = data;
    if( callback ) return callback();
});

const computedStyle = curry(( elem, style ) => {
    const currentStyle = prop( "currentStyle", elem );
    return currentStyle
        ? setProp( "currentStyle", elem, style )
        : getComputedStyle( elem, null )[ style ];
});

const toggleBtn = btn => btn.disabled = !btn.disabled;

const zeroPadTime = num => num < 10 ? "0" + num : num;

const durationCounter = elem => {
    let counter = 0, seconds = 0, minutes = 0, hours = 0;

    return setInterval(() => {
        seconds = zeroPadTime( counter % 60 );
        minutes = zeroPadTime( Math.floor( counter / 60 ));
        hours = zeroPadTime( Math.floor(( counter / 60 ) / 60 ));
        printData( elem, `${ hours }:${ minutes }:${ seconds }` );
        counter += 1;
    }, 1000 );
};

// User

const sessionTime = curry(( duration, startTime ) => {
    const firstIdx = duration.indexOf( ":" );
    const lastIdx = duration.lastIndexOf( ":" );
    const seconds = duration.slice( lastIdx + 1 ) * 1000;
    const minutes = duration.slice( firstIdx + 1, lastIdx ) * 60 * 1000;
    const hours = duration.slice( 0, firstIdx ) * 60 * 60 * 1000;

    return {
        endTime : new Date( startTime.getTime() + seconds + minutes + hours ),
        sessionLength : duration
    };
});

const parseTimeStr = time => time.toString().replace(
    /(.+\d{2}:\d{2}:\d{2}).+/,
    "$1"
);


const user = () => {
    return {
        startTime : null,
        questions : 1,
        answers : 0,
        currentQuestion : null,
        incorrect : {},

        getTime( duration ){
            const startT = parseTimeStr( this.startTime );

            if( duration ){
                const session = sessionTime( duration, this.startTime );
                const endT = parseTimeStr( prop( "endTime", session ));
                return {
                    startTime : startT,
                    endTime : endT,
                    sessionLength : prop( "sessionLength", session )
                };
            }

            return startT;
        },
        setStartTime(){ setProp( "startTime", this, new Date()); },
        getQuestionNum(){ return prop( "questions", this ); },
        getAnswerNum(){ return prop( "answers", this ); },
        getCurrentQuestion(){ return prop( "currentQuestion", this ); },
        setCurrentQuestion( lineObj ){ this.currentQuestion = lineObj; },
        setAnswerNum(){ this.answers += 1; },
        setQuestionNum(){ this.questions += 1; },
        setIncorrectAnswer( line ){ 
            this.incorrect[ line ] = this.incorrect[ line ] + 1 || 1; 
        },
        getIncorrectAnswer(){
            let line, num = 0;
            for( let ans in this.incorrect ){
                if( this.incorrect[ ans ] > num ){
                    line = ans;
                    num = this.incorrect[ ans ];
                }
            }
            return line;
        }
    };
};

// I/O functions

const testFile = lines => lines.some( item => /[^|]+[|]{1,}[^|]+/.test( item ));

const replaceMarkDown = ln => {
    // remove trailing list token and spaces
    ln = ln.replace( /(^[*\s]+)?(\S+)(\s+$)?/, '$2');
    // replace ** or __ with bold tags
    ln = ln.replace( /[*_]{2}([^|*_]+)[*_]{2}/g, "<b>$1</b>" );
    // replace * or _ with italic tags
    ln = ln.replace( /[*_]([^|*_]+)[*_]/g, '<i>$1</i>');
    // remove spaces either side of |
    return ln.replace( /(\s{0,}\|\s{0,})/, "|" );
};

const fileObj = curry(( callback, file ) => {
    const reader = new FileReader();
    let wholeText, lines;

    reader.onload = function( event ){
        wholeText = event.target.result;
        lines = wholeText.split( "\n" );

        if( testFile( lines ))
            callback( true );
        else
            callback( false );
    };

    reader.readAsText( file );

    return { 
        getText(){ return wholeText; },
        getLines(){ return lines; },
        getRandomLine(){ 
            const rndNum = Math.floor( Math.random() * lines.length );
            if( lines[ rndNum ].indexOf( "|" ) > 0 ){
                const ln = replaceMarkDown( lines[ rndNum ]);
                return { 
                    eng : ln.slice( 0, ln.indexOf( "|" )),
                    spn : ln.slice( ln.indexOf( "|" ) + 1 )
                };
            } else
                return this.getRandomLine();
        }
    };
});

// App Functions

const generateLink = line => {
    const url = "http://www.spanishdict.com/translate/";
    const convertLine = encodeURIComponent( line );
    const audio = "http://audio1.spanishdict.com/audio?lang=es&text=es%C3%A1s-bien";
    return url + convertLine;
};

const generateAudio = line => { 
    const url = "http://audio1.spanishdict.com/audio?lang=es&text=";
    const encodeLine = encodeURIComponent(line);
    return url + encodeLine;
};

const playAudio = elem => {
    elem.play();
};

const pronunciation = ( clickElem, audio ) => {
    clickEvent( clickElem, event => {
        event.preventDefault();
        if( !clickElem.classList.contains( "disabled" ))
            playAudio( audio );
    });
};

const fileInput = ( fileIn, fileObj ) => {
    eventListener( "change", fileIn, () => {
        const filename = fileIn.value.replace( "C:\\fakepath\\", "" );
        fileIn.parentElement.getElementsByClassName( "label" )[ 0 ].classList.remove( "hide" );
        printData( fileIn.parentElement.getElementsByClassName( "file_name" )[ 0 ], ` ${ filename }` );
        fileObj[ 1 ] = fileObj[ 0 ]( fileIn.files[ 0 ]);
    });
};

const enableStart = curry(( startBtn, errorMsg, status ) => {
    if( status ){
        setProp( "disabled", startBtn, false );
        startBtn.classList.remove( "error_btn" );
        startBtn.classList.add( "rubberBand", "correct_btn" );
    } else {
        toggleClass( errorMsg, "hide" );
        startBtn.classList.remove( "correct_btn", "rubberBand" );
        startBtn.classList.add( "error_btn" );
        setProp( "disabled", startBtn, true );
    }
});

const uploadBtn = ( upBtn, fileInput ) => {
    clickEvent( upBtn, () => {
        fileInput.click();
    });
};

const languageToggle = ( engLink, spnLink, questionCont, answerCont ) => {
    clickEvent( spnLink, event => {
        event.preventDefault();
        spnLink.classList.remove( "disabled" );
        spnLink.classList.add( "active" );
        engLink.classList.remove( "active" );
        questionCont.classList.add( "hide" );
        answerCont.classList.remove( "hide" );
    });

    clickEvent( engLink, event => {
        event.preventDefault();
        engLink.classList.add( "active" );
        spnLink.classList.remove( "active" );
        questionCont.classList.remove( "hide" );
        answerCont.classList.add( "hide" );
    });
};

const getQuestionAndAnswer = curry(( engLink, spnLink, answerBtn, questionCont, answerCont, fileObj, user ) => {
    const question = fileObj[ 1 ].getRandomLine();
    const questionNum = user[ 0 ].getQuestionNum();
    user[ 0 ].setCurrentQuestion( question );
    engLink.click();
    spnLink.classList.add( "disabled" );
    answerBtn.classList.remove( "hide" );
    printData( questionCont, question.eng );
    printData( answerCont, question.spn );

    return user;
});

const setExternalLink = curry(( user, audioElem, externalLink ) => {
    const spn = user[ 0 ].getCurrentQuestion().spn;
    externalLink.href = generateLink( spn );
    audioElem.src = generateAudio( spn );
    toggleClass( externalLink, "disabled" );
});

const answerBtn = ( ansBtn, externalLink, spnLink, correctAnsBtn, incorrectAnsBtn, callback ) => {
    clickEvent( ansBtn, () => {
        callback( externalLink );
        spnLink.click();
        correctAnsBtn.classList.remove( "hide" );
        incorrectAnsBtn.classList.remove( "hide" );
        ansBtn.classList.add( "hide" );
    });
};

const ansAction = curry(( correctAnsBtn, incorrectAnsBtn, externalLink, user, callback, status ) => {
    toggleClass( correctAnsBtn, "hide" );
    toggleClass( incorrectAnsBtn, "hide" );
    toggleClass( externalLink, "disabled" );
    user[ 0 ].setQuestionNum();
    
    if( status )
        user[ 0 ].setAnswerNum();
    else
        user[ 0 ].setIncorrectAnswer( user[ 0 ].getCurrentQuestion().spn ); 

    callback( user );
});

const answerCorrect = curry(( correctAnsBtn, incorrectAnsBtn, callback ) => {
    clickEvent( correctAnsBtn, () => { callback( true ); });
    clickEvent( incorrectAnsBtn, () => { callback( false ); });
});

const setTimer = ( durationElem, timer ) => timer[ 0 ] = durationCounter( durationElem );

const endTimer = timer => {
    clearInterval( timer[ 0 ] );
};

const printQuestion = curry(( questNumElem, user ) => {
    return printData( questNumElem, user[ 0 ].getQuestionNum(), () => user );
});

const printAnswer = curry(( ansNumElem, user ) => {
    return printData( ansNumElem, user[ 0 ].getAnswerNum(), () => user );
});

const printStartTime = curry(( startTimeElem, user ) => {
    return printData( startTimeElem, user[ 0 ].getTime(), () => user );
});

const getResults = curry(( durationElem, user ) => {
    let timings = user[ 0 ].getTime( durationElem.innerHTML );
    let phrase = user[ 0 ].getIncorrectAnswer();
    let percentage = user[ 0 ].getAnswerNum() / ( user[ 0 ].getQuestionNum() - 1 );
    
    phrase = !phrase ? "You got them all right! Or perhaps all wrong…" : phrase;
    percentage = isNaN( percentage ) 
        ? "0%"
        : percentage === 1
            ? "100%"
        : percentage.toFixed( 2 ).slice( 2 ) + "%";

    if( typeof timings === "string" )
        timings = {
            startTime : timings, 
            endTime : timings, 
            sessionLength : "It ended all so quickly"
        };

    return { phrase, percentage, timings };
});

const populatePhrases = curry(( phrasesElem, fileObj ) => {
    console.log( phrasesElem );
    const lineArr = fileObj[ 1 ].getLines();
    const lineArrLength = lineArr.length - 1;
    const lineStr = lineArr.reduce(( str, ln, pos ) => {
        if( pos === 0 ) str += `<p>${ ln }</br>`;
        else if( pos === lineArrLength ) str += `${ ln }</p>`;
        else str += `${ ln }</br>`;

        return str;
    }, "" );
    printData( phrasesElem, lineStr );
});

const populateResults = curry(( fileObj, resPhrases, resQuestion, resAnswer, resPercentage, resDuration, resStart, resEnd, user, resultsObj ) => {
    printData( resPhrases, resultsObj.phrase );
    printData( resQuestion, user[ 0 ].getQuestionNum() - 1 );
    printData( resAnswer, user[ 0 ].getAnswerNum());
    printData( resPercentage, resultsObj.percentage );
    printData( resDuration, resultsObj.timings.sessionLength );
    printData( resStart, resultsObj.timings.startTime );
    printData( resEnd, resultsObj.timings.endTime );

    return fileObj; // only exists to return to another function, poor
});

const reviewPhrases = ( reviewPhrasesElemClass, reviewPhrasesElemId, closePhrasesElem ) => {
    clickEvent( reviewPhrasesElemId, () => {
        toggleClass( reviewPhrasesElemClass, "show" );
        document.body.style.overflow = "hidden";
    });

    clickEvent( closePhrasesElem, () => {
        toggleClass( reviewPhrasesElemClass, "show" );
        document.body.style.overflow = "auto";
    });

};

const startAgain = ( usr, startAgainBtn, sectionWelcome, fileInput, reviewPhrases ) => {
    clickEvent( startAgainBtn, () => {
        toggleSections( sectionWelcome, "show" );
        usr[ 0 ] = user();
        fileInput.value = null;
        reviewPhrases.classList.remove( "reveal" );
        document.body.style.overflow = "auto";
    });
};

const closeErrorMsg = ( closeErrorBtn, errorMsg ) => {
    clickEvent( closeErrorBtn, () => { toggleClass( errorMsg, "hide" ); });
};

// Style functions

const styleBtns = ( light, dark, pink ) => {
    const btnArr = [ light, dark, pink ];
    const htmlElem = document.documentElement;
    const warningElem = document.getElementById( "theme_warning" );
    let warningText = true;

    warningElem.addEventListener( "click", () => {
        warningText = false;
        localStorage.setItem( "SFC_warning", 1 );
        warningElem.classList.remove( "display" );
    });

    function switchStyles(){
        btnArr.forEach( btn => {
            btn.classList.remove( "active", "rubberBand" );
        });

        this.classList.add( "active", "rubberBand" );
        if( this.id === "light_style" ){
            htmlElem.setAttribute( "data-theme", "light" );
            localStorage.setItem( "SFC_theme", "" );
        } else if( this.id === "dark_style" ){
            htmlElem.setAttribute( "data-theme", "dark" );
            localStorage.setItem( "SFC_theme", "dark" );
            localStorage.theme = "dark";
        } else {
            htmlElem.setAttribute( "data-theme", "pink" );
            localStorage.setItem( "SFC_theme", "pink" );
        }
        if( warningText ){
            const lsPref = localStorage.getItem( "SFC_warning" );
            if( !lsPref && lsPref !== 1 ) 
                warningElem.classList.add( "display" );
        }
    };

    btnArr.forEach( btn => clickEvent( btn, switchStyles ));
};

const changeFont = ( script, sans, serif, paperText, changeFontElem ) => {
    const fontArr = [ script, sans, serif ];
    clickEvent( changeFontElem, event => {
        event.preventDefault();
        const target = event.target;
        const id = target.id;
        fontArr.forEach( font => font.classList.remove( "active" ));
        const textClass = ( cla ) => paperText.forEach( t => {
            t.classList.remove( "sans" );
            t.classList.remove( "serif" );
            t.classList.add( cla );
        });

        target.classList.add( "active" );

        if( id === "script" ){
            textClass( "script" );
            localStorage.setItem( "SFC_font", "script" );
        } else if( id === "sans" ){
            textClass( "sans" );
            localStorage.setItem( "SFC_font", "sans" );
        } else {
            textClass( "serif" );
            localStorage.setItem( "SFC_font", "serif" );
        }
    });
};

const checkReviewBtnStyle = ( reviewPhrases, startAgainBtn ) => {
    if( computedStyle( reviewPhrases, "display" ) === "none" )
        startAgainBtn.classList.remove( "left_border_btn" );
    else 
        startAgainBtn.classList.add( "left_border_btn" );
};

const start = ( timer, startBtn, sectionTest, dataElem, durationElem, fileInput, user, callback ) => {
    clickEvent( startBtn, () => {
        startBtn.classList.remove( "rubberBand" );
        toggleSections( sectionTest, "show" );
        toggleBtn( startBtn );
        toggleClass( dataElem, "show" );
        user[ 0 ].setStartTime();
        setTimer( durationElem, timer );
        fileInput.parentElement.getElementsByClassName( "label" )[ 0 ].classList.add( "hide" );
        printData( fileInput.parentElement.getElementsByClassName( "file_name" )[ 0 ], "" );
        callback( user );
    });
};

const end = ( endSessionBtn, sectionResults, dataElem, externalLink, user, timer, reviewPhrases, startAgainBtn, callback ) => {
    clickEvent( endSessionBtn, () => {
        callback( user );
        toggleSections( sectionResults, "show" );
        externalLink.classList.add( "disabled" );
        toggleClass( dataElem, "show" );
        endTimer( timer );
        checkReviewBtnStyle( reviewPhrases, startAgainBtn );
    }); 
};

const readLocalStorage = ( light, dark, pink, script, sans, serif, paperText ) => {
    const htmlElem = document.documentElement;
    const currentTheme = localStorage.getItem( "SFC_theme" );
    const currentFont = localStorage.getItem( "SFC_font" );
    const textClass = ( cla ) => paperText.forEach( t => t.classList.add( cla ));
    [ script, sans, serif ].forEach( f => f.classList.remove( "active" ));

    switch ( currentFont ){
        case "sans" :
            textClass( "sans" );
            sans.classList.add( "active" );
            break;
        case "serif" :
            textClass( "serif" );
            serif.classList.add( "active" );
            break;
        default :
            script.classList.add( "active" );
            break;
    }


    switch( currentTheme ){
        case "dark" :
            dark.classList.add( "active" );
            light.classList.remove( "active" );
            htmlElem.setAttribute( "data-theme", "dark" );
            break;
        case "pink" :
            pink.classList.add( "active" );
            light.classList.remove( "active" );
            htmlElem.setAttribute( "data-theme", "pink" );
            break;
        default :
            break;
    }
};


// DOM Elements
// Welcome page style buttons
const light = $( "#light_style" );
const dark = $( "#dark_style" );
const pink = $( "#pink_style" );

// Upload and Start Buttons
const fileIO = $( "#file" );
const upload = $( "#upload" );
const startBtn = $( "#start" );
const errorMsg = $( "#error_msg" );
const dataElem = $( "#data" );
const sectionTest = $( "#language_test" );

// Test Section
const engLink = $( "#showEng" );
const spnLink = $( "#showSpn" );
const ansBtn = $( "#getAnswer" );
const questionCont = $( "#question" );
const answerCont = $( "#answer" );
const ansNumElem = $( ".answer_num" );
const questNumElem = $( ".question_num" );
const startTimeElem = $( ".start_time" );
const durationElem = $( ".duration" );
const externalLink = $( "#spnDictLink" );
const audioElem = $( "#listen_to_spanish" );
const correctAnsBtn = $( "#correctAns" );
const incorrectAnsBtn = $( "#incorrectAns" );
const startAgainBtn = $( "#startAgain" );
const sectionWelcome = $( "#welcome_upload_file" );
const closeErrorBtn = $( "#close_error_msg" );
const endSessionBtn = $( "#endSession" );
const sectionResults = $( "#test_results" );

// Results Section
const reviewSection = $( ".reviewPhrases" );
const reviewPhrasesElem = $( "#reviewPhrases" );
const closePhrases = $( "#closePhrases" );
const resPhrases = $( ".results_phrases" );
const resQuestion = $( ".results_question" );
const resAnswer = $( ".results_answer" );
const resPercentage = $( ".results_percentage" );
const resDuration = $( ".results_duration" );
const resStart = $( ".results_start" );
const resEnd = $( ".results_end" );
const paperText = $( ".text", true );
const changeFontElem = $( "#change_font" );
const script = $( "#script" );
const serif = $( "#serif" );
const sans = $( "#sans" );

// Initialise objects for constant use
// Massive kludge to get these variables to update within
// the above functions, otherwise passed as value
let $user = {
    0 : user()
};

const $file = {
    0 : fileObj( enableStart( startBtn, errorMsg )),
    1 : null
};

let timer = {
    0 : null
};

// Compositions

const getAndPrintQA = compose( 
    printQuestion( questNumElem ),
    printAnswer( ansNumElem ),
    getQuestionAndAnswer( engLink, spnLink, ansBtn, questionCont, answerCont, $file )
);

const startCB = compose(
    printStartTime( startTimeElem ),
    getAndPrintQA,
);

const resultsComp = compose(
    populatePhrases( paperText[ 1 ]),
    populateResults( 
        $file,
        resPhrases, 
        resQuestion, 
        resAnswer, 
        resPercentage, 
        resDuration, 
        resStart, 
        resEnd, 
        $user 
    ),
    getResults( durationElem ) // requires userObj
);

readLocalStorage( light, dark, pink, script, sans, serif, paperText );
styleBtns( light, dark, pink );

fileInput( fileIO, $file );
uploadBtn( upload, fileIO );

start( timer, startBtn, sectionTest, dataElem, durationElem, fileIO, $user, startCB );
languageToggle( engLink, spnLink, questionCont, answerCont );
answerBtn( 
    ansBtn, 
    externalLink, 
    spnLink, 
    correctAnsBtn, 
    incorrectAnsBtn,
    setExternalLink( $user, audioElem )
);
answerCorrect( correctAnsBtn, incorrectAnsBtn, 
    ansAction( 
        correctAnsBtn, 
        incorrectAnsBtn,
        externalLink,
        $user,
        getAndPrintQA
    )
);
closeErrorMsg( closeErrorBtn, errorMsg );
startAgain( $user, startAgainBtn, sectionWelcome, fileIO, reviewPhrasesElem );
pronunciation( externalLink, audioElem );
end( endSessionBtn, sectionResults, dataElem, externalLink, $user, timer, reviewPhrasesElem, startAgainBtn, resultsComp );

reviewPhrases( reviewSection, reviewPhrasesElem, closePhrases );
changeFont( script, sans, serif, paperText, changeFontElem );
