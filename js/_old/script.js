/* Utility Object 
	Contains basic functions
*/
var utilities = {
	//toggle class for the element passed in
	toggleClass : function(elem, classname){
		if(elem.tagName == 'SECTION'){
			var sections = document.getElementsByTagName('section');
			for(var i = 0; i < sections.length; i++){
				if(sections[i] !== elem){
					sections[i].classList.remove(classname);
				}
			}
		}
		if(elem.classList.contains(classname)){
			elem.classList.remove(classname);
		}else{
			elem.classList.add(classname);
		}
	},
	//toggle btn 
	toggleBtn : function(btn){
		if(btn.disabled){
			btn.disabled = false;
		}else{
			btn.disabled = true;
		}
	},
	//generate counter
	durationCounter : function(elem){
		var counter = 0, seconds = 0, minutes = 0, hours = 0;
		return setInterval(function(){
			seconds = counter % 60;
			minutes = Math.floor(counter / 60);
			hours = Math.floor((counter / 60) / 60);
			seconds = seconds < 10 ? '0' + seconds : seconds;
			minutes = minutes < 10 ? '0' + minutes : minutes;
			hours = hours < 10 ? '0' + hours : hours;
			elem.innerHTML = hours+":"+minutes+":"+seconds;
			counter = counter + 1;
		}, 1000);
	},
	//get computed style
	computedStyle : function(elem, style){
		return elem.currentStyle ? elem.currentStyle[style] : getComputedStyle(elem, null)[style];
	}
};

/*User Singleton

	Created once a session is started. Contains information 
	regarding the number of questions, the amount of correct 
	answers, percentage rating, time started, time ended, session length, phrases that have been answered incorrectly and tallied
*/
var user = function(){
	//Private Variables
	var startTime = new Date(), endTime, sessionLength;
	var questionObj = {
		questions : 1,
		answers : 0
	};


	//Private Functions
	function sessionTime(duration){
		var seconds = duration.slice(duration.lastIndexOf(':')+1) * 1000;
		var minutes = duration.slice(duration.indexOf(':') + 1, duration.lastIndexOf(':')) * 60 * 1000;
		var hours = duration.slice(0, duration.indexOf(':')) * 60 * 60 * 1000;
		endTime = new Date(startTime.getTime() + seconds + minutes + hours);
		sessionLength = duration;
	}

	return{
		getTime : function(duration){
			var startT = startTime.toString().replace(/(.+\d{2}:\d{2}:\d{2}).+/, '$1');
			if(duration){
				sessionTime(duration);
				var endT = endTime.toString().replace(/(.+\d{2}:\d{2}:\d{2}).+/, '$1');
				return [startT, endT, sessionLength];
			}
			return startT;
			
		},
		//get question num
		getQuestionNum : function(){
			return questionObj.questions;
		},
		//get answer number
		getAnswerNum : function(){
			return questionObj.answers;
		},
		setAnswerNum : function(){
			questionObj.answers = questionObj.answers + 1;
		},
		setQuestionNum : function(){
			questionObj.questions = questionObj.questions + 1;
		},
		//add incorrect answer to object
		setIncorrectAnswer : function(line){
			questionObj[line] = questionObj[line] + 1 || 1;
		},
		//test function - is this working correctly
		getIncorrectAnswersObj : function(){
			return questionObj;
		},
		//check for most incorrect answer 
		getIncorrectAnswers : function(){
			var line, num = 0;
			for(var ans in questionObj){
				if(ans !== 'questions' && ans !== 'answers'){
					if(questionObj[ans] > num){
						line = ans;
						num = questionObj[ans];
					}
				}
			}
			return line;
		}
	};
};

/*File Singleton
 
	Accepts a file and stores the relevant information,
	accessed consistantly during the question section of the
	site.
*/
var fileObj = function(file, startBtn, error_msg){
	//Private variables
	var wholeText, lines, reader = new FileReader();
	//Private Functions
	reader.onload = function(event){
		//set variables once FileReader has finished loading
		wholeText = event.target.result;
		lines = wholeText.split('\n');
		if(testFile()){
			//good to go!
			utilities.toggleBtn(startBtn);
			startBtn.classList.add('rubberBand');
			startBtn.classList.remove('error_btn');
			startBtn.classList.add('correct_btn');
		}else{
			//file not valid!
			utilities.toggleClass(error_msg, 'hide');
			startBtn.classList.remove('correct_btn');
			startBtn.classList.add('error_btn');
			startBtn.classList.remove('rubberBand');
			startBtn.disabled = true;
		}
	};
	//Read file as text using FileReader
	reader.readAsText(file);
	//Replace markdown syntax
	function replaceMd(line){
		//remove astrix and spaces at start and spaces at end
		line = line.replace(/(^[*\s]+)?(\S+)(\s+$)?/, '$2');
		//remove space before and after pipe
		line = line.replace(/(([^|]+)[\s]{0,}[|]{1,}[\s]{0,}([^|]+))/, '$2|$3');
		//convert astrixs into italics tag
		return line.replace(/\(\*([^|]+)\*\)/g, ' (<i>$1</i>)');
	}
	//function loop over each line and check if there are valid lines - reject otherwise
	function testFile(){
		return lines.some(function(item){
			return /[^|]+[\s]{0,}[|]{1,}[\s]{0,}[^|]+/.test(item);
		});	
	}
	//Return object with privileged methods
	return {
		//return the entire text file contents
		getText : function(){
			return wholeText;
		},
		//return array with text split by the line
		getLines : function(){
			return lines;
		},
		//get random line, excluding those which have no pipe
		getRandomLine : function(){
			var randomNumber = Math.floor(Math.random() * lines.length);
			if(lines[randomNumber].indexOf('|') > 0){
				var line = replaceMd(lines[randomNumber]);
				return {eng : line.slice(0, line.indexOf('|') - 1), spn : line.slice(line.indexOf('|') + 1)};
			}else{
				return this.getRandomLine();
			}	
		}
	};
};

/*App Class

	The object that is created when the page first loads containing functions for running the site
*/

var app = function(options){
	/*Private Variables*/
	var fileObject, userObject, question, elements = {}, timer;
	/*Private Functions*/
	//grab all the ids in the options obj and target them in DOM, saved to a elements obj
	function getElements(){
		for(var i in options){
			elements[i] = document.getElementById(options[i]);
		}
	}
	function getQuestionAndAnswer(){
		question = fileObject.getRandomLine();
		var questionNum = userObject.getQuestionNum();
		elements.engLink.click(); //go back to english
		elements.spnLink.classList.add('disabled'); //disable spanish button
		elements.spnLink.classList.remove('active');
		elements.engLink.classList.add('active');
		elements.answerBtn.classList.remove('hide'); //hide answer
		//elements.questionCont.innerHTML = '<span class="circleNum">' + questionNum + '</span>' + question.eng;
		elements.questionCont.innerHTML = question.eng;
		elements.answerCont.innerHTML = question.spn;
	}
	//generate link to spanish/english dictionary
	function generateLink(line){
		var url = "http://www.spanishdict.com/translate/";
		var convertLine = encodeURIComponent(line);
		var audio = "http://audio1.spanishsict.com/audio?lang=es&text=est%C3%A1s-bien";
		return url + convertLine;
	}
	//generate url for audio pronunciation
	function generateAudio(line){
		var url = "http://audio1.spanishdict.com/audio?lang=es&text=";
		var encodeLine = encodeURIComponent(line);
		return url + encodeLine;
	}
	//trigger hidden audio element
	function pronunciation(){
		elements.externalLink.addEventListener('click', function(event){
			event.preventDefault();
			if(!this.classList.contains('disabled')){
				elements.audioElem.play(); //trigger hidden audio element
			}
		});
	}
	//target input elem, listen to changes, grab file and create fileObj instance
	function fileInput(){
		elements.fileInput.addEventListener('change', function(){
			fileObject = fileObj(this.files[0], elements.startBtn, elements.error_msg);
			var filename = this.value.replace('C:\\fakepath\\', '');
			this.parentElement.getElementsByClassName('label')[0].classList.remove('hide');
			this.parentElement.getElementsByClassName('file_name')[0].innerHTML = filename;
		});
	}
	//trigger file upload
	function uploadBtn(){
		elements.upload.addEventListener('click', function(){
			elements.fileInput.click();
		});	
	}
	//toggle between english and spanish text
	function languageToggle(){
		elements.spnLink.addEventListener('click', function(event){
			event.preventDefault();
			this.classList.remove('disabled');
			this.classList.add('active');
			elements.engLink.classList.remove('active');
			elements.questionCont.classList.add('hide');
			elements.answerCont.classList.remove('hide');
		});

		elements.engLink.addEventListener('click', function(event){
			event.preventDefault();
			elements.questionCont.classList.remove('hide');
			elements.answerCont.classList.add('hide');
			elements.spnLink.classList.remove('active');
			this.classList.add('active');
		});
	}
	//reveal the answer
	function answerBtn(){
		elements.answerBtn.addEventListener('click', function(){
			elements.externalLink.href = generateLink(question.eng);
			elements.audioElem.src = generateAudio(question.spn);
			elements.externalLink.classList.remove('disabled');
			elements.spnLink.click(); //reveal spanish
			elements.correctAnsBtn.classList.remove('hide'); //reveal buttons
			elements.incorrectAnsBtn.classList.remove('hide');
			this.classList.add('hide'); //hide this button

		});
	}
	function answerCor(){
		elements.correctAnsBtn.addEventListener('click', function(){
			ansAction(true);
		});
		elements.incorrectAnsBtn.addEventListener('click', function(){
			ansAction(false);
		});
	}
	function ansAction(cor){
		//hide correct/incorrect buttons and pronounciation link
		elements.correctAnsBtn.classList.add('hide');
		elements.incorrectAnsBtn.classList.add('hide');
		elements.externalLink.classList.add('disabled');
		//update question number
		userObject.setQuestionNum();
		if(cor){ //if correct answer
			userObject.setAnswerNum(); //update score
		}else{
			//add spanish to incorrect phrases object
			userObject.setIncorrectAnswer(question.spn);
		}
		printQuestion();
		printAnswer();
		getQuestionAndAnswer(); //get new line
	}
	//start the duration counter
	function setTimer(){
		var duration = elements.header.getElementsByClassName('duration')[0];
		timer = utilities.durationCounter(duration);
	}
	//end the duration counter
	function endTimer(){
		clearInterval(timer);
	}
	//print question number
	function printQuestion(){
		elements.header.getElementsByClassName('question_num')[0].innerHTML = userObject.getQuestionNum();
	}
	//print answer number
	function printAnswer(){
		elements.header.getElementsByClassName('answer_num')[0].innerHTML = userObject.getAnswerNum();
	}
	//print start time
	function printStartTime(){
		elements.header.getElementsByClassName('start_time')[0].innerHTML = userObject.getTime();
	}
	//get results and check values
	function getResults(){
		var timings = userObject.getTime(elements.header.getElementsByClassName('duration')[0].innerHTML);
		var phrase = userObject.getIncorrectAnswers();
		var percentage = (userObject.getAnswerNum() / (userObject.getQuestionNum() - 1));
		//check if phrase is a false boolean
		phrase = !phrase ? "You got them all right! Or perhaps wrong…" : phrase;
		//check if it's a number
		if(isNaN(percentage)){
			percentage = '0%';
		}else{
			percentage = percentage === 1 ? '100%' : percentage.toFixed(2).slice(2) + "%";
		}
		//check if we got a string value 
		if(typeof timings == 'string'){
			timings = [timings, timings, 'It ended all so quickly'];
		}

		var ul = elements.sectionResults.getElementsByClassName('results_list')[0];
		populateResults(ul, phrase, percentage, timings);
	}
	//get text array, add tags and insert into elem
	function populatePhrases(elem){
		var linesArr = fileObject.getLines();
		linesArr.forEach(function(item, pos){
			if(pos === 0){
				linesArr[pos] = '<p>' + linesArr[pos] + '<br>';
			}else if(pos === linesArr.length - 1){
				linesArr[pos] = linesArr[pos] + '</p>';
			}else{
				linesArr[pos] = linesArr[pos] + '<br>';
			}
		});
		elem.innerHTML = linesArr.join('');
	}
	//populate results section
	function populateResults(ul, phrase, percentage, timings){
		ul.getElementsByClassName('results_phrases')[0].innerHTML = phrase;
		ul.getElementsByClassName('results_question')[0].innerHTML = (userObject.getQuestionNum() - 1);
		ul.getElementsByClassName('results_answer')[0].innerHTML = userObject.getAnswerNum();
		ul.getElementsByClassName('results_percentage')[0].innerHTML = percentage;
		ul.getElementsByClassName('results_duration')[0].innerHTML = timings[2];
		ul.getElementsByClassName('results_start')[0].innerHTML = timings[0];
		ul.getElementsByClassName('results_end')[0].innerHTML = timings[1];
		//get whole text to review
		populatePhrases(elements.sectionResults.getElementsByClassName('text')[0]);
	}
	//review phrases
	function reviewPhrases(){
		var reviewText = elements.sectionResults.getElementsByClassName('reviewPhrases')[0];
		elements.reviewPhrases.addEventListener('click', function(){
			utilities.toggleClass(reviewText, 'show');
			document.body.style.overflow = 'hidden';
		});
		elements.closePhrases.addEventListener('click', function(){
			utilities.toggleClass(reviewText, 'show');
			document.body.style.overflow = 'auto';
		});
	}
	//restart
	function startAgain(){
		elements.startAgainBtn.addEventListener('click', function(){
			utilities.toggleClass(elements.sectionWelcome, 'show');
			elements.fileInput.value = null;
			elements.reviewPhrases.classList.remove('reveal');
			document.body.style.overflow = 'auto';
		});
	}
	function closeErrorMsg(){
		elements.closeErrorBtn.addEventListener('click', function(){
			utilities.toggleClass(elements.error_msg, 'hide');
		});
	}
	//switch between styles
	function styleBtn(){
		function switchStyle(){
			this.classList.add('active', 'rubberBand');
			if(this.id === 'light_style'){
				this.nextElementSibling.classList.remove('active', 'rubberBand');
				this.nextElementSibling.nextElementSibling.classList.remove('active', 'rubberBand');
				document.body.classList.remove('dark', 'pink');
				localStorage.theme = null;
			}else if(this.id === 'dark_style'){
				this.previousElementSibling.classList.remove('active', 'rubberBand');
				this.nextElementSibling.classList.remove('active', 'rubberBand');
				document.body.classList.remove('pink');
				document.body.classList.add('dark');
				localStorage.theme = 'dark';
			}else{
				this.previousElementSibling.classList.remove('active', 'rubberBand');
				this.previousElementSibling.previousElementSibling.classList.remove('active', 'rubberBand');
				document.body.classList.remove('dark');
				document.body.classList.add('pink');
				localStorage.theme = 'pink';
			}
		}
		elements.lightBtn.addEventListener('click', switchStyle);
		elements.darkBtn.addEventListener('click', switchStyle);
		elements.pinkBtn.addEventListener('click', switchStyle);
	}
	//switch font styles for review phrases - this function is terribly written
	function changeFont(){
		var script = document.getElementById('script');
		var sans = document.getElementById('sans');
		var serif = document.getElementById('serif');
		var paperText = elements.sectionResults.getElementsByClassName('text')[0];

		elements.changeFont.addEventListener('click', function(event){
			event.preventDefault();
			script.classList.remove('active');
			sans.classList.remove('active');
			serif.classList.remove('active');

			if(event.target.id === 'script'){
				event.target.classList.add('active');
				paperText.classList.remove('sans', 'serif');
			}else if(event.target.id === 'serif'){
				event.target.classList.add('active');
				paperText.classList.remove('script', 'sans');
				paperText.classList.add('serif');
			}else if(event.target.id === 'sans'){
				event.target.classList.add('active');
				paperText.classList.remove('script', 'serif');
				paperText.classList.add('sans');
			}
		});
	}
	//check if review btn is displayed and add/remove style accordingly
	function checkReviewBtnStyle(){
		if(utilities.computedStyle(elements.reviewPhrases, 'display') === 'none'){
			elements.startAgainBtn.classList.remove('left_border_btn');
		}else{
			elements.startAgainBtn.classList.add('left_border_btn');
		}
	}
	//generate new user and start question
	function start(){
		elements.startBtn.addEventListener('click', function(){
			userObject = user();
			elements.startBtn.classList.remove('rubberBand');
			utilities.toggleClass(elements.sectionTest, 'show');
			utilities.toggleBtn(elements.startBtn);
			utilities.toggleClass(elements.data, 'show');
			getQuestionAndAnswer();
			setTimer();
			printQuestion();
			printAnswer();
			printStartTime();
			//remove some styles
			elements.fileInput.parentElement.getElementsByClassName('label')[0].classList.add('hide');
			elements.fileInput.parentElement.getElementsByClassName('file_name')[0].innerHTML = '';
		});
	}
	//quit current session
	function end(){
		document.getElementById(options.endSessionBtn).addEventListener('click', function(){
			getResults();
			utilities.toggleClass(elements.sectionResults, 'show');
			utilities.toggleClass(elements.data, 'show');
			elements.externalLink.classList.add('disabled');
			endTimer();
			checkReviewBtnStyle();
		});
	}
	/*Privileged Object Functions*/
	return { //check localStorage and set styles accordingly
		readLocalStorage : function(){
			if(localStorage.theme && localStorage.theme === 'dark'){
				document.getElementById('dark_style').classList.add('active');
				document.getElementById('light_style').classList.remove('active');
				document.body.classList.add('dark');
			}else if(localStorage.theme && localStorage.theme === 'pink'){
				document.getElementById('light_style').classList.remove('active');
				document.getElementById('pink_style').classList.add('active');
				document.body.classList.add('pink');
			}
		},
		//ready, set, go!
		init : function(){
			getElements();
			fileInput();
			uploadBtn();
			start();
			languageToggle();
			answerBtn();
			answerCor();
			closeErrorMsg();
			startAgain();
			reviewPhrases();
			end();
			changeFont();
			styleBtn();
			pronunciation();
		}
	};
};

//Page init
(function(){
	var ap = app({
		//html element ID's
		fileInput : 'file',
		upload : 'upload',
		startBtn : 'start',
		header : 'main_header',
		data : 'data',
		questionCont : 'question',
		answerCont : 'answer',
		answerBtn : 'getAnswer',
		startAgainBtn : 'startAgain',
		endSessionBtn : 'endSession',
		correctAnsBtn : 'correctAns',
		incorrectAnsBtn : 'incorrectAns',
		closeErrorBtn : 'close_error_msg',
		lightBtn : 'light_style',
		darkBtn : 'dark_style',
		pinkBtn : 'pink_style',
		error_msg : 'error_msg',
		reviewPhrases : 'reviewPhrases',
		closePhrases : 'closePhrases',
		engLink : 'showEng',
		spnLink : 'showSpn',
		externalLink : 'spnDictLink',
		sectionWelcome : 'welcome_upload_file',
		sectionTest : 'language_test',
		sectionResults : 'test_results',
		changeFont : 'change_font',
		audioElem : 'listen_to_spanish'
		//add more!
	});
	ap.readLocalStorage();
	ap.init();
})();
