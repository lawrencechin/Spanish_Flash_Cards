# Spanish Flash Cards 🇪🇸

![Screenshot](./screenshot.jpg)

A simple flash card site. It was originally built for learning *Spanish* but one can use any file as long as it's in the correct format. Upload a `.txt` or `.md` file that features lists like so:

```markdown

* Question | Answer
* Question | Answer

```
